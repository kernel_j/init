# README #
This project is an introduction to system administration, network, and DevOps.
The goal is to discover common commands used by sysadmin as well as some
services used on server machines.

The project is divided into 3 parts: Network, Systems, and Scripts.

### Network
This section is done on Mac OSX

### System
This section is done on a virtual machine running Debian

### Scripts
This section is done on a virtual machine running Debian
